// Dart imports:
import 'dart:async';

// Project imports:
import 'package:better_player/src/configuration/better_player_controls_configuration.dart';
import 'package:better_player/src/controls/better_player_clickable_widget.dart';
import 'package:better_player/src/controls/better_player_controls_state.dart';
import 'package:better_player/src/controls/better_player_material_progress_bar.dart';
import 'package:better_player/src/controls/better_player_progress_colors.dart';
import 'package:better_player/src/core/better_player_controller.dart';
import 'package:better_player/src/core/better_player_utils.dart';
import 'package:better_player/src/video_player/video_player.dart';

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'better_player_clickable_widget.dart';

class BetterPlayerMaterialControls extends StatefulWidget {
  ///Callback used to send information if player bar is hidden or not
  final Function(bool visbility) onControlsVisibilityChanged;

  ///Controls config
  final BetterPlayerControlsConfiguration controlsConfiguration;

  const BetterPlayerMaterialControls({
    Key key,
    @required this.onControlsVisibilityChanged,
    @required this.controlsConfiguration,
  })  : assert(onControlsVisibilityChanged != null),
        assert(controlsConfiguration != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BetterPlayerMaterialControlsState();
  }
}

class _BetterPlayerMaterialControlsState
    extends BetterPlayerControlsState<BetterPlayerMaterialControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  Timer _hideTimer;
  Timer _initTimer;
  Timer _showAfterExpandCollapseTimer;
  bool _displayTapped = false;
  bool _wasLoading = false;
  VideoPlayerController _controller;
  BetterPlayerController _betterPlayerController;
  StreamSubscription _controlsVisibilityStreamSubscription;
  bool fullScreen = false;

  BetterPlayerControlsConfiguration get _controlsConfiguration =>
      widget.controlsConfiguration;

  @override
  VideoPlayerValue get latestValue => _latestValue;

  @override
  BetterPlayerController get betterPlayerController => _betterPlayerController;

  @override
  BetterPlayerControlsConfiguration get betterPlayerControlsConfiguration =>
      _controlsConfiguration;

  @override
  Widget build(BuildContext context) {
    _wasLoading = isLoading(_latestValue);
    if (_latestValue?.hasError == true) {
      return Container(
        color: Colors.black,
        child: _buildErrorWidget(),
      );
    }
    return MouseRegion(
      onHover: (_) {
        cancelAndRestartTimer();
      },
      child: Container(
        decoration: BoxDecoration(
            color: _hideStuff
                ? Colors.transparent
                : Colors.black.withOpacity(0.5)),
        child: GestureDetector(
          onTap: () {
            _hideStuff
                ? cancelAndRestartTimer()
                : setState(() {
                    _hideStuff = true;
                  });
          },
          onDoubleTap: () {
            cancelAndRestartTimer();
            _onPlayPause();
          },
          child: AbsorbPointer(
            absorbing: _hideStuff,
            child: Column(
              children: [
                _buildTopBar(),
                if (_wasLoading)
                  Expanded(child: Center(child: _buildLoadingWidget()))
                else
                  _buildHitArea(),
                _buildBottomBar(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    _controller?.removeListener(_updateState);
    _hideTimer?.cancel();
    _initTimer?.cancel();
    _showAfterExpandCollapseTimer?.cancel();
    _controlsVisibilityStreamSubscription?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = _betterPlayerController;
    _betterPlayerController = BetterPlayerController.of(context);
    _controller = _betterPlayerController.videoPlayerController;
    _latestValue = _controller.value;

    if (_oldController != _betterPlayerController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildErrorWidget() {
    Size size = MediaQuery.of(context).size;
    final errorBuilder =
        _betterPlayerController.betterPlayerConfiguration.errorBuilder;
    if (errorBuilder != null) {
      return errorBuilder(context,
          _betterPlayerController.videoPlayerController.value.errorDescription);
    } else {
      // final textStyle = TextStyle(color: _controlsConfiguration.textColor);
      return Center(
          child: _controlsConfiguration.enableRetry
              ? ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Material(
                    color: Colors.transparent,
                    child: IconButton(
                      onPressed: () {
                        _betterPlayerController.retryDataSource();
                      },
                      icon: Icon(Icons.replay, color: Colors.white),
                      iconSize: MediaQuery.of(context).orientation ==
                              Orientation.portrait
                          ? size.height / 15
                          : size.height / 7,
                    ),
                  ),
                )
              : Container());
    }
  }

  Widget _buildTopBar() {
    Size size = MediaQuery.of(context).size;

    if (!betterPlayerController.controlsEnabled) {
      return const SizedBox();
    }

    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: _controlsConfiguration.controlsHideTime,
      onEnd: _onPlayerHide,
      child: Container(
        width: size.width,
        // decoration: BoxDecoration(
        //     gradient: LinearGradient(
        //   begin: Alignment.topCenter,
        //   end: Alignment.bottomCenter,
        //   colors: [Colors.black, Colors.transparent],
        // )),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          betterPlayerController.betterPlayerConfiguration.showCloseButton !=
                      null ||
                  betterPlayerController
                          .betterPlayerConfiguration.showCloseButton ==
                      true
              ? Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Material(
                      color: Colors.transparent,
                      child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          }),
                    ),
                  ),
                )
              : Container(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            width: size.width / 2,
            child: Center(
              child: Text(
                  betterPlayerController
                                  .betterPlayerConfiguration.titleContent ==
                              null ||
                          betterPlayerController.betterPlayerConfiguration
                                  .titleContent.length ==
                              0
                      ? ""
                      : betterPlayerController
                          .betterPlayerConfiguration.titleContent,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: MediaQuery.of(context).orientation ==
                            Orientation.landscape
                        ? 22
                        : 17,
                  )),
            ),
          ),
          if (_controlsConfiguration.enablePip)
            _buildPipButtonWrapperWidget(_hideStuff, _onPlayerHide)
          else
            const SizedBox(),
          if (_controlsConfiguration.enableOverflowMenu)
            AnimatedOpacity(
              opacity: _hideStuff ? 0.0 : 1.0,
              duration: _controlsConfiguration.controlsHideTime,
              onEnd: _onPlayerHide,
              child: Container(
                height: _controlsConfiguration.controlBarHeight,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    betterPlayerController
                                .betterPlayerConfiguration.chromeCastButton !=
                            null
                        ? betterPlayerController
                            .betterPlayerConfiguration.chromeCastButton
                        : Container(),
                    _buildMoreButton(),
                  ],
                ),
              ),
            )
          else
            const SizedBox()
        ]),
      ),
    );
  }

  Widget _buildPipButton() {
    return BetterPlayerMaterialClickableWidget(
      onTap: () {
        betterPlayerController.enablePictureInPicture(
            betterPlayerController.betterPlayerGlobalKey);
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Icon(
          betterPlayerControlsConfiguration.pipMenuIcon,
          color: betterPlayerControlsConfiguration.iconsColor,
        ),
      ),
    );
  }

  Widget _buildPipButtonWrapperWidget(
      bool hideStuff, void Function() onPlayerHide) {
    return FutureBuilder<bool>(
      future: betterPlayerController.isPictureInPictureSupported(),
      builder: (context, snapshot) {
        final bool isPipSupported = snapshot.data ?? false;
        if (isPipSupported &&
            _betterPlayerController.betterPlayerGlobalKey != null) {
          return AnimatedOpacity(
            opacity: hideStuff ? 0.0 : 1.0,
            duration: betterPlayerControlsConfiguration.controlsHideTime,
            onEnd: onPlayerHide,
            child: Container(
              height: betterPlayerControlsConfiguration.controlBarHeight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  _buildPipButton(),
                ],
              ),
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Widget _buildMoreButton() {
    return BetterPlayerMaterialClickableWidget(
      onTap: () {
        onShowMoreClicked();
      },
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Icon(
          _controlsConfiguration.overflowMenuIcon,
          color: _controlsConfiguration.iconsColor,
        ),
      ),
    );
  }

  Widget _buildBottomBar() {
    if (!betterPlayerController.controlsEnabled) {
      return const SizedBox();
    }
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: _controlsConfiguration.controlsHideTime,
      onEnd: _onPlayerHide,
      child: Container(
        // decoration: BoxDecoration(
        //     gradient: LinearGradient(
        //   begin: Alignment.bottomCenter,
        //   end: Alignment.topCenter,
        //   colors: [Colors.black, Colors.transparent],
        // )),

        height: _controlsConfiguration.controlBarHeight,
        // color: _controlsConfiguration.controlBarColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (_controlsConfiguration.enablePlayPause &&
                !_betterPlayerController.isLiveStream())
              _buildPlayPause(_controller)
            else
              const SizedBox(),
            // if (_betterPlayerController.isLiveStream())
            //   _buildLiveWidget()
            // else
            _controlsConfiguration.enableProgressText
                ? _buildPosition()
                : const SizedBox(),
            if (_betterPlayerController.isLiveStream())
              const SizedBox()
            else
              _controlsConfiguration.enableProgressBar
                  ? _buildProgressBar()
                  : const SizedBox(),
            if (_controlsConfiguration.enableMute)
              _buildMuteButton(_controller)
            else
              const SizedBox(),
            if (_controlsConfiguration.enableFullscreen)
              _buildExpandButton()
            else
              const SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget _buildLiveWidget() {
    return Expanded(
      child: Text(
        _betterPlayerController.translations.controlsLive,
        style: TextStyle(
            color: _controlsConfiguration.liveTextColor,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildExpandButton() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Material(
        color: Colors.transparent,
        clipBehavior: Clip.hardEdge,
        child: IconButton(
            icon: Icon(
              MediaQuery.of(context).orientation == Orientation.landscape
                  // _betterPlayerController.isFullScreen
                  ? _controlsConfiguration.fullscreenDisableIcon
                  : _controlsConfiguration.fullscreenEnableIcon,
              color: _controlsConfiguration.iconsColor,
            ),
            onPressed: _onExpandCollapse),
      ),
    );

    // return BetterPlayerMaterialClickableWidget(
    //   onTap: _onExpandCollapse,
    //   child: AnimatedOpacity(
    //     opacity: _hideStuff ? 0.0 : 1.0,
    //     duration: _controlsConfiguration.controlsHideTime,
    //     child: Container(
    //       // height: _controlsConfiguration.controlBarHeight,
    //       margin: const EdgeInsets.only(right: 12.0),
    //       padding: const EdgeInsets.symmetric(horizontal: 8.0),
    //       child: Center(
    //         child: Icon(
    //           MediaQuery.of(context).orientation == Orientation.landscape
    //               // _betterPlayerController.isFullScreen
    //               ? _controlsConfiguration.fullscreenDisableIcon
    //               : _controlsConfiguration.fullscreenEnableIcon,
    //           color: _controlsConfiguration.iconsColor,
    //         ),
    //       ),
    //     ),
    //   ),
    // );
  }

  Widget _buildHitArea() {
    if (!betterPlayerController.controlsEnabled) {
      return const SizedBox();
    }
    return Expanded(
      child: Container(
        color: Colors.transparent,
        child: Center(
          child: AnimatedOpacity(
            opacity: _hideStuff ? 0.0 : 1.0,
            duration: _controlsConfiguration.controlsHideTime,
            child: Stack(
              children: [
                _buildMiddleRow(),
                // _buildNextVideoWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMiddleRow() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _betterPlayerController
                      .betterPlayerConfiguration.skipPreviousButton !=
                  null
              ? customButton(_betterPlayerController
                  .betterPlayerConfiguration.skipPreviousButton)
              :
              // if (_controlsConfiguration.enableSkips &&
              //     !_betterPlayerController.isLiveStream())
              //   _buildSkipButton()
              const SizedBox(),
          _buildReplayButton(),
          _betterPlayerController.betterPlayerConfiguration.skipNextsButton !=
                  null
              ? customButton(_betterPlayerController
                  .betterPlayerConfiguration.skipNextsButton)
              // if (_controlsConfiguration.enableSkips &&
              //     !_betterPlayerController.isLiveStream())
              //   _buildForwardButton()
              : const SizedBox(),
        ],
      ),
    );
  }

  Widget customButton(Widget childParameter) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Material(
        color: Colors.transparent,
        clipBehavior: Clip.hardEdge,
        child: childParameter,
      ),
    );
  }

  Widget _buildHitAreaClickableButton(
      {Widget icon, void Function() onClicked}) {
    return BetterPlayerMaterialClickableWidget(
      onTap: onClicked,
      child: Align(
        child: Container(
          decoration: BoxDecoration(
            // color: _controlsConfiguration.controlBarColor,
            borderRadius: BorderRadius.circular(48),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Stack(
              children: [icon],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSkipButton() {
    return _buildHitAreaClickableButton(
      icon: Icon(
        Icons.replay_10,
        size: 32,
        color: _controlsConfiguration.iconsColor,
      ),
      onClicked: skipBack,
    );
  }

  Widget _buildForwardButton() {
    return _buildHitAreaClickableButton(
      icon: Icon(
        Icons.forward_10,
        size: 32,
        color: _controlsConfiguration.iconsColor,
      ),
      onClicked: skipForward,
    );
  }

  Widget _buildReplayButton() {
    final bool isFinished = isVideoFinished(_latestValue);
    if (!isFinished) {
      return const SizedBox();
    }

    return _buildHitAreaClickableButton(
      icon: Icon(
        Icons.replay,
        size: 32,
        color: _controlsConfiguration.iconsColor,
      ),
      onClicked: () {
        if (_latestValue != null && _latestValue.isPlaying) {
          if (_displayTapped) {
            setState(() {
              _hideStuff = true;
            });
          } else {
            cancelAndRestartTimer();
          }
        } else {
          _onPlayPause();

          setState(() {
            _hideStuff = true;
          });
        }
      },
    );
  }

  Widget _buildNextVideoWidget() {
    return StreamBuilder<int>(
      stream: _betterPlayerController.nextVideoTimeStreamController.stream,
      builder: (context, snapshot) {
        final time = snapshot.data;
        if (time != null && time > 0) {
          return BetterPlayerMaterialClickableWidget(
            onTap: () {
              _betterPlayerController.playNextVideo();
            },
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: const EdgeInsets.only(bottom: 4, right: 24),
                decoration: BoxDecoration(
                  color: _controlsConfiguration.controlBarColor,
                  borderRadius: BorderRadius.circular(32),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Text(
                    "${_betterPlayerController.translations.controlsNextVideoIn} $time ...",
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Widget _buildMuteButton(
    VideoPlayerController controller,
  ) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Material(
        color: Colors.transparent,
        clipBehavior: Clip.hardEdge,
        child: IconButton(
            icon: Icon(
              (_latestValue != null && _latestValue.volume > 0)
                  ? _controlsConfiguration.muteIcon
                  : _controlsConfiguration.unMuteIcon,
              color: _controlsConfiguration.iconsColor,
            ),
            onPressed: () {
              cancelAndRestartTimer();
              if (_latestValue.volume == 0) {
                _betterPlayerController.setVolume(_latestVolume ?? 0.5);
              } else {
                _latestVolume = controller.value.volume;
                _betterPlayerController.setVolume(0.0);
              }
            }),
      ),
    );
    // return BetterPlayerMaterialClickableWidget(
    //   onTap: () {
    //     cancelAndRestartTimer();
    //     if (_latestValue.volume == 0) {
    //       _betterPlayerController.setVolume(_latestVolume ?? 0.5);
    //     } else {
    //       _latestVolume = controller.value.volume;
    //       _betterPlayerController.setVolume(0.0);
    //     }
    //   },
    //   child: AnimatedOpacity(
    //     opacity: _hideStuff ? 0.0 : 1.0,
    //     duration: _controlsConfiguration.controlsHideTime,
    //     child: ClipRect(
    //       child: Container(
    //         height: _controlsConfiguration.controlBarHeight,
    //         padding: const EdgeInsets.symmetric(horizontal: 8),
    //         child: Icon(
    //           (_latestValue != null && _latestValue.volume > 0)
    //               ? _controlsConfiguration.muteIcon
    //               : _controlsConfiguration.unMuteIcon,
    //           color: _controlsConfiguration.iconsColor,
    //         ),
    //       ),
    //     ),
    //   ),
    // );
  }

  Widget _buildPlayPause(VideoPlayerController controller) {
    return BetterPlayerMaterialClickableWidget(
      onTap: _onPlayPause,
      child: Container(
        height: _controlsConfiguration.controlBarHeight,
        margin: const EdgeInsets.symmetric(horizontal: 4),
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Icon(
          controller.value.isPlaying
              ? _controlsConfiguration.pauseIcon
              : _controlsConfiguration.playIcon,
          color: _controlsConfiguration.iconsColor,
        ),
      ),
    );
  }

  Widget _buildPosition() {
    final position = _latestValue != null && _latestValue.position != null
        ? _latestValue.position
        : Duration.zero;
    final duration = _latestValue != null && _latestValue.duration != null
        ? _latestValue.duration
        : Duration.zero;

    return Padding(
      padding: const EdgeInsets.only(right: 24),
      child: Text(
        '${BetterPlayerUtils.formatDuration(position)} / ${BetterPlayerUtils.formatDuration(duration)}',
        style: TextStyle(
          fontSize: 14,
          color: _controlsConfiguration.textColor,
          decoration: TextDecoration.none,
        ),
      ),
    );
  }

  @override
  void cancelAndRestartTimer() {
    _hideTimer?.cancel();
    _startHideTimer();

    setState(() {
      _hideStuff = false;
      _displayTapped = true;
    });
  }

  Future<void> _initialize() async {
    _controller.addListener(_updateState);

    _updateState();

    if ((_controller.value != null && _controller.value.isPlaying) ||
        _betterPlayerController.betterPlayerConfiguration.autoPlay) {
      _startHideTimer();
    }

    if (_controlsConfiguration.showControlsOnInitialize) {
      _initTimer = Timer(const Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }

    _controlsVisibilityStreamSubscription =
        _betterPlayerController.controlsVisibilityStream.listen((state) {
      setState(() {
        _hideStuff = !state;
      });
      if (!_hideStuff) {
        cancelAndRestartTimer();
      }
    });
  }

  void _onExpandCollapse() {
    // setState(() {
    //   _hideStuff = true;

    //   _betterPlayerController.toggleFullScreen();
    //   _showAfterExpandCollapseTimer =
    //       Timer(_controlsConfiguration.controlsHideTime, () {
    //     setState(() {
    //       cancelAndRestartTimer();
    //     });
    //   });
    // });

    if (fullScreen == false) {
      setState(() {
        fullScreen = true;
        _hideStuff = true;

        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);
      });
    } else if (fullScreen) {
      setState(() {
        fullScreen = false;
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
      });
    }
  }

  void _onPlayPause() {
    bool isFinished = false;

    if (_latestValue?.position != null && _latestValue?.duration != null) {
      isFinished = _latestValue.position >= _latestValue.duration;
    }

    setState(() {
      if (_controller.value.isPlaying) {
        _hideStuff = false;
        _hideTimer?.cancel();
        _betterPlayerController.pause();
      } else {
        cancelAndRestartTimer();

        if (!_controller.value.initialized) {
        } else {
          if (isFinished) {
            _betterPlayerController.seekTo(const Duration());
          }
          _betterPlayerController.play();
          _betterPlayerController.cancelNextVideoTimer();
        }
      }
    });
  }

  void _startHideTimer() {
    if (_betterPlayerController.controlsAlwaysVisible) {
      return;
    }
    _hideTimer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _updateState() {
    if (mounted) {
      if (!_hideStuff ||
          isVideoFinished(_controller.value) ||
          _wasLoading ||
          isLoading(_controller.value)) {
        setState(() {
          _latestValue = _controller.value;
          if (isVideoFinished(_latestValue)) {
            _hideStuff = false;
          }
        });
      }
    }
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(right: 20),
        child: BetterPlayerMaterialVideoProgressBar(
          _controller,
          _betterPlayerController,
          onDragStart: () {
            _hideTimer?.cancel();
          },
          onDragEnd: () {
            _startHideTimer();
          },
          colors: BetterPlayerProgressColors(
              playedColor: _controlsConfiguration.progressBarPlayedColor,
              handleColor: _controlsConfiguration.progressBarHandleColor,
              bufferedColor: _controlsConfiguration.progressBarBufferedColor,
              backgroundColor:
                  _controlsConfiguration.progressBarBackgroundColor),
        ),
      ),
    );
  }

  void _onPlayerHide() {
    _betterPlayerController.toggleControlsVisibility(!_hideStuff);
    widget.onControlsVisibilityChanged(!_hideStuff);
  }

  Widget _buildLoadingWidget() {
    if (_controlsConfiguration.loadingWidget != null) {
      return _controlsConfiguration.loadingWidget;
    }

    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(
          _controlsConfiguration.loadingColor ??
              _controlsConfiguration.controlBarColor),
    );
  }
}
